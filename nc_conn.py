# %%
import subprocess
import io
import sys
import time
from datetime import timedelta

if __name__ == '__main__':

    port = 20000
    processes = []
    for x in range(1, len(sys.argv)):
        sensor = "m3-" + str(sys.argv[x])
        print("Sensor: %s at port %s" % (str(sensor), str(port)))
        # input_file = open(str(sensor) + '.in', mode='a+b')
        output_file = open(str(sensor) + '.out', mode='a+b')
        process = subprocess.Popen(["nc", str(sensor), str(port)], stdout=output_file, stdin=sys.stdin)
        processes.append(process)

    while True:
        pass
