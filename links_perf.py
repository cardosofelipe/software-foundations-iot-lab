# %%
import pandas as pd
import ast
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import glob

TYPE_TX = 'TX'
TYPE_RX = 'RX'


def get_dataframes(file='nc.out'):
    with open(file) as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    df_rx = pd.DataFrame(
        columns=['batch', 'to', 'from', 'rssi', 'current_packet', 'total', 'sent_time', 'received_time'])
    df_tx = pd.DataFrame(columns=['node_id', 'batch', 'sent', 'total', 'start_time', 'end_time'])
    for c in content:
        try:
            d = ast.literal_eval(c)
        except:
            continue

        # if "current" in d.keys():
        if d['type'] == 'RX':
            df_rx.loc[len(df_rx)] = [d['batch'], d['to'], d['from'], d['rssi'], d['current_packet'], d['total'],
                                     d['sent_time'],
                                     d['received_time']]
        if d['type'] == 'TX':
            df_tx.loc[len(df_tx)] = [d['node_id'], d['batch'], d['sent'], d['total'], d['start_time'], d['end_time']]

    return df_tx, df_rx


def analyze(sensors):
    graph = nx.DiGraph()
    # data_tx_dfs = []
    # data_rx_dfs = []
    #
    data_rx = pd.DataFrame(
        columns=['batch', 'to', 'from', 'rssi', 'current_packet', 'total', 'sent_time', 'received_time'])
    data_tx = pd.DataFrame(columns=['node_id', 'batch', 'sent', 'total', 'start_time', 'end_time'])
    for s in sensors:
        print("Reading data of sensor: %s" % s)
        d_tx, d_rx = get_dataframes(str(s))
        if not d_tx.empty:
            data_tx = pd.concat([data_tx, d_tx])
        if not d_rx.empty:
            data_rx = pd.concat([data_rx, d_rx])

    # data_tx = pd.concat(data_tx_dfs)
    # data_rx = pd.concat(data_rx_dfs)

    stats = pd.DataFrame(
        columns=['from', 'to', 'mean_rssi', 'sent', 'received', 'throughput', 'tx_rate', 'rx_rate', 'mean_tx_time',
                 'mean_rx_time'])

    # batches = data_rx['batch'].unique()
    ids = set()
    ids.update(data_rx['from'].unique())
    ids.update(data_rx['to'].unique())
    ids.update(data_tx['node_id'].unique())

    print("Building stats and graph...")

    for i in ids:
        graph.add_node(i)
        for j in ids:
            if i >= j:
                continue
            graph.add_node(j)

            curr_rx_a = data_rx[(data_rx['from'] == i) & (data_rx['to'] == j)]
            curr_rx_b = data_rx[(data_rx['from'] == j) & (data_rx['to'] == i)]

            curr_tx_a = data_tx[data_tx['node_id'] == i]
            curr_tx_b = data_tx[data_tx['node_id'] == j]

            mean_rssi_a = curr_rx_a['rssi'].mean()
            mean_rssi_b = curr_rx_b['rssi'].mean()

            batches_a = curr_tx_a['batch'].unique()
            batches_b = curr_tx_b['batch'].unique()

            sent_a = curr_tx_a['sent'].sum()
            sent_b = curr_tx_b['sent'].sum()

            total_a = curr_tx_a['total'].sum()
            total_b = curr_tx_b['total'].sum()

            received_a = len(curr_rx_a)
            received_b = len(curr_rx_b)

            througputs_a = []
            througputs_b = []

            rx_times_a = []
            tx_times_a = []

            rx_times_b = []
            tx_times_b = []

            for batch in batches_a:
                curr_tx_batch_a = curr_tx_a[curr_tx_a['batch'] == batch]
                curr_rx_batch_a = curr_rx_a[curr_rx_a['batch'] == batch]

                curr_duration = curr_tx_batch_a['end_time'].iloc[0] - curr_tx_batch_a['start_time'].iloc[0]

                print("curr duration %d" % curr_duration)
                batch_sent = curr_tx_batch_a['sent'].iloc[0]
                print("curr batch sent: %d" % batch_sent)

                througputs_a.append(batch_sent / curr_duration)

                curr_rx_time = curr_rx_batch_a['received_time'].diff().mean()
                if curr_rx_time is not np.nan:
                    rx_times_a.append(curr_rx_time)

                curr_tx_time = curr_rx_batch_a['sent_time'].diff().mean()
                if curr_tx_time is not np.nan:
                    tx_times_a.append(curr_tx_time)

            for batch in batches_b:
                curr_tx_batch_b = curr_tx_b[curr_tx_b['batch'] == batch]
                curr_rx_batch_b = curr_rx_b[curr_rx_b['batch'] == batch]

                curr_duration = curr_tx_batch_b['end_time'].iloc[0] - curr_tx_batch_b['start_time'].iloc[0]
                batch_sent = curr_tx_batch_b['sent'].iloc[0]
                print("curr batch sent: %d" % batch_sent)

                througputs_b.append(batch_sent / curr_duration)

                curr_rx_time = curr_rx_batch_b['received_time'].diff().mean()
                if curr_rx_time is not np.nan:
                    rx_times_b.append(curr_rx_time)

                curr_tx_time = curr_rx_batch_b['sent_time'].diff().mean()
                if curr_tx_time is not np.nan:
                    tx_times_b.append(curr_tx_time)

            print("throghputs a " + str(througputs_a))
            print("throghputs b " + str(througputs_b))

            if len(througputs_a) is 0:
                througput_a = 0
            else:
                througput_a = np.mean(througputs_a)

            if len(througputs_b) is 0:
                througput_b = 0
            else:
                througput_b = np.mean(througputs_b)

            print("rx_times a " + str(rx_times_a))
            print("rx_times b " + str(rx_times_b))
            print("tx_times a " + str(tx_times_a))
            print("tx_times b " + str(tx_times_b))

            rx_time_a = 0 if len(rx_times_a) == 0 else np.mean(rx_times_a)
            tx_time_a = 0 if len(tx_times_a) == 0 else np.mean(tx_times_a)

            rx_time_b = 0 if len(rx_times_b) == 0 else np.mean(rx_times_b)
            tx_time_b = 0 if len(tx_times_b) == 0 else np.mean(tx_times_b)

            if total_a is 0:
                tx_rate_a = 0
                rx_rate_a = 0
            else:
                tx_rate_a = sent_a / total_a
                rx_rate_a = received_a / total_a

            if total_b is 0:
                tx_rate_b = 0
                rx_rate_b = 0
            else:
                tx_rate_b = sent_b / total_b
                rx_rate_b = received_b / total_b

            # columns = ['from', 'to', 'mean_rssi', 'sent', 'received', 'throughput', 'tx_rate', 'rx_rate',
            #            'mean_tx_time',
            #            'mean_rx_time'])

            if len(curr_rx_a) > 0:
                graph.add_edge(i, j, weight=mean_rssi_a, tp=througput_a, rx_rate=rx_rate_a, tx_rate=tx_rate_a)

            if len(curr_rx_b) > 0:
                graph.add_edge(j, i, weight=mean_rssi_b, tp=througput_b, rx_rate=rx_rate_b, tx_rate=tx_rate_b)

            stats.loc[len(stats)] = [i, j, mean_rssi_a, sent_a, received_a, througput_a, tx_rate_a, rx_rate_a,
                                     rx_time_a, tx_time_a]

            stats.loc[len(stats)] = [j, i, mean_rssi_b, sent_b, received_b, througput_b, tx_rate_b, rx_rate_b,
                                     rx_time_b, tx_time_b]

    print("Done!")

    return stats, graph


def draw_graph(G):
    pos = nx.circular_layout(graph)
    nx.draw(graph, pos, with_labels=True, font_color='blue', font_size=9, node_size=600, font_weight='bold')
    node_list = G.nodes

    # Uncomment this to add custom labels to the nodes, remove the withLabels from draw

    # labels = {}
    # for node_name in node_list:
    #     labels[str(node_name)] = str(node_name)
    # nx.draw_networkx_labels(G, pos, labels, font_size=16)

    all_weights = []
    # Iterate through the graph nodes to gather all the weights
    for (node1, node2, data) in G.edges(data=True):
        all_weights.append(data['weight'])  # we'll use this when determining edge thickness

    # Get unique weights
    unique_weights = list(set(all_weights))

    # Plot the edges - one by one!
    for weight in unique_weights:
        weighted_edges = [(node1, node2) for (node1, node2, edge_attr) in G.edges(data=True) if
                          edge_attr['weight'] == weight]
        width = 2 * abs(weight) * len(node_list) * 3.0 / abs(sum(all_weights))
        nx.draw_networkx_edges(G, pos, edgelist=weighted_edges, width=width, arrowsize=15, arrowstyle='<-')
        # Uncomment the code below for writing stats on the edges

        # edge_labels = dict([((u, v,),
        #                      "rssi:" + str(d['weight'])
        #                      + "\nTP:" + "%.3f" % d['tp']
        #                      + "\ntx_rate:" + "%.2f%%" % (d['tx_rate'] * 100)
        #                      + "\nrx_rate:" + "%.2f%%" % (d['rx_rate'] * 100))
        #                     for u, v, d in G.edges(data=True)])
        # nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, label_pos=0.3, font_size=8, font_weight='normal')


# %%
files = glob.glob("./Data/data_4_nodes/*.out")
print(files)
stats, graph = analyze(files)
draw_graph(graph)
plt.show()
